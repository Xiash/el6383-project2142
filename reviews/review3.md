
Project Review
=====================================================


## Overview

1) Briefly summarize the experiment in this project.

	The experiment tries to evaluate the performance degradation in using flowsetup in SDN networks.
	This is achieved by pinging two different SDN networks that use and do not use flowsetup and measuring the average RTT time in both netwworks.


2) Does the project generally follow the guidelines and parameters we have
learned in class?

	Yes, The project follows the general guidelines learnt in class.
	The goal is narrow and the parameters varied are in line with the experiment. Qualitave measurements would have
	made the project more tangible.


## Experiment design

1) What is the goal of this experiment? Is it a focused, specific goal?
Is it useful and likely to have interesting results?

	The project tries to establish the existence of the delay created by flowsetup in SDNs.
	Even though exact quantative measurements are not taken, the project in overall shows qualitatively
	the effect that flowsetup has on link establishment.
	the goal is narrow and specific. The result is not likely to have interesting results since the SDN flowsetup WILL always
	create delay and no quantitative analysis of the delay is made. By ensuring quantitative results, the autor could have made
	the results more useful.

2) Are the metric(s) and parameters chosen appropriate for this
experiment goal? Does the experiment design clearly support the experiment goal?

	The metric (RTT) is appropriate to measure the experiment goal (delay due to flowsetup).
	The parameter varied is the existence of flowsetup and also stays true to the goal.
	thus, yes the design clearly supports the experiment goal.


3) Is the experiment well designed to obtain maximum information with the
minimum number of trials?

	RTT is measured by pinging and average RTT is acheived by running the ping command 100 times.
	Multiple trials are run, but it is all done via a single command. Hence, they cannot be counted as multiple trials in my opinion.

4) Are the metrics selected for study the *right* metrics? Are they clear,
unambiguous, and likely to lead to correct conclusions? Are there other
metrics that might have been better suited for this experiment?

	Yes, The metric RTT selected for the study is the right metric. It is clear, unambigous and leads to the correct conclusion.
	Throughput might have been a better metric for this experiment.


5) Are the parameters of the experiment meaningful? Are the ranges
over which parameters vary meaningful and representative?


6) Have the authors sufficiently addressed the possibility of interactions
between parameters?

	There is only one parameter and hence there is no possibility of interaction between both and hence the
	author does not mention it.

7) Are comparisons made reasonably? Is the baseline selected for comparison appropriate
and realistic?

	The comparisons are made reasonably and the baseline selected is realistic and apporpriate.


## Communicating results


1) Do the authors report the quantitative results of their experiment?

	No.


2) Is there information given about the variation and/or distribution of
experimental results?

	The author does mention about the variation and/or distribution of the result
	IT is taken care of, by using the average RTT value.

3) Do the authors practice *data integrity* - telling the truth about their data,
avoiding ratio games and other practices to artificially make their results seem better?

	Data Integritiy is highly maintained in the experiment.

4) Is the data presented in a clear and effective way? If the data is presented in
graphical form, is the type of graph selected appropriate for the "story" that
the data is telling?

	Yes.
	The bar graph is an highly effective tool to visualize data in this experiment.


5) Are the conclusions drawn by the authors sufficiently supported by the
experiment results?

	Yes, the conclusions drawn are very well supported by the data from the experiment.

## Reproducible research



1) Did the authors include instructions for reproducing the experiment in 3 ways (Raw data -> Results,
Existing experiment setup -> Data, and Set up experiment)? Are the instructions clear
and easy to understand?

	Yes, all 3 methods were included by the author.
	Clear and easy to understand commands were also provided.

2) Were you able to successfully produce experiment results?

	Yes.


3) How long did it take you to run this experiment, from start to finish?

	Apart from reserving the resources, the experiment took 30 minutes to complete.


4) Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?

	No changes were needed.

5) In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?

	Level 5 reproducible experiment.


## Other comments to authors

Please write any other comments that you think might help the authors
of this project improve their experiment.

	A publicly routable IP address is only needed to log in to web version of R and this can be avoided by creating a .R script in host1 using vi(or any other text editor)
	thus negating the need for installing R, pushing and pulling data from the repository in the controller.
