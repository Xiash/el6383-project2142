
Project Review
=====================================================

## Overview

1) Briefly summarize the experiment in this project.


This project is about SDN. The group of students builded a small scale of SDN,used the ping results to find the RTTs from one host to another, and verified the existence of extra latency introduced by the flow setup process. Also, they used R to plot the difference of the RTTs with and without flowsetup.


2) Does the project generally follow the guidelines and parameters we have
learned in class?


Yes. They followed the guidelines and the parameter they varied is whether there has a routing entry in the switch for the ping packets from one host to the other, which should definitely be the parameter varied.



## Experiment design

1) What is the goal of this experiment? Is it a focused, specific goal?
Is it useful and likely to have interesting results?


The goal of this experiment is to find the significant latency in SDN caused by the flow setup process.
It's a very focused and specific goal.
The result is kind of predictable, since we have known this latency from lecture.


2) Are the metric(s) and parameters chosen appropriate for this
experiment goal? Does the experiment design clearly support the experiment goal?


The matrics for this project were the different RTTs in different situations, which were essential to this project.  
The parameter changed for this experiment can be consider as two situations, one is packet transporting with flow setup process, and another is transporting without the setup process. Since what we want to verify is the latency by flow setup, this parameter chosen supports the experiment.
From above, we can say the eperiment design supports the goal.


3) Is the experiment well designed to obtain maximum information with the
minimum number of trials?


Yes. You can get the result by one trial. And to be more confirmed, you can conduct several trials.


4) Are the metrics selected for study the *right* metrics? Are they clear,
unambiguous, and likely to lead to correct conclusions? Are there other
metrics that might have been better suited for this experiment?


Since what the experiment trying to find is the latency, RTT as metrics is the first choise.
RTT can directly be got by ping results. This makes the metric clear and direct. And using RTT to show the latency is definitely correct.


5) Are the parameters of the experiment meaningful? Are the ranges
over which parameters vary meaningful and representative?


Although the parameter is not something that can be manually controlled, it is meanningful and representative.
Since what this experiment trying to measure were RTTs with and without flow setup, the only parameter that need to be varied is the transporting process with flow setup and without.


6) Have the authors sufficiently addressed the possibility of interactions
between parameters?


There's only one parameter varied in this experiment, so no interactions need to be considered.


7) Are comparisons made reasonably? Is the baseline selected for comparison appropriate
and realistic?


Comparisons for this experiment were reasonable.  Comparison was based on one trial with limited variant, so it's appropriate.



## Communicating results


1) Do the authors report the quantitative results of their experiment?


The authors reported one bar graph for their result.


2) Is there information given about the variation and/or distribution of
experimental results?


Yes. The graph was polted based on the variation of the situations.


3) Do the authors practice *data integrity* - telling the truth about their data,
avoiding ratio games and other practices to artificially make their results seem better?

Yes. Their data was the real output of the ping result. Plus, my reproduce of the experiment also leads to the same result.


4) Is the data presented in a clear and effective way? If the data is presented in
graphical form, is the type of graph selected appropriate for the "story" that
the data is telling?


The bar graph is a clear and effective way to present the result. The only insufficient is that they only show the result for one trial.


5) Are the conclusions drawn by the authors sufficiently supported by the
experiment results?


Yes. There's obvious difference between the RTTs with and without flow setup process. Therefore, the conclusion about latency triggered by flow setup is fully supported by the experiment's result.


## Reproducible research



1) Did the authors include instructions for reproducing the experiment in 3 ways (Raw data -> Results,
Existing experiment setup -> Data, and Set up experiment)? Are the instructions clear
and easy to understand?


The instructions provided by the author are really clear and straight forward. Just following the steps can lead you from the setting up of the topology to the exact result.

2) Were you able to successfully produce experiment results?
Yes.

Here's their result:

![](http://ww4.sinaimg.cn/bmiddle/889060e3gw1errqm7z5epj218e0ta76h.jpg)

And here's my result:

![](http://ww2.sinaimg.cn/bmiddle/889060e3gw1errqm9bnrej21kw0u877a.jpg)

The two graphs are nearly the same. I can easily find the latency caused by flow setup process.

3) How long did it take you to run this experiment, from start to finish?


It took me about 2 to 3 hours.


4) Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?


The only difference I made reproducing this experiment was that I changed the aggregate they used, NYU InstaGENI, to Utah InstaGENI, because I couldn't login in to the nodes with NYU InstaGENI. However, this had nothig to do with their designment of this experiment and had only some ignorable influences of the whole experiment.
It only took about extra 20 mins.

5) In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?


4


## Other comments to authors

Please write any other comments that you think might help the authors
of this project improve their experiment.


This whole project is concise and effective. Although only a small topology was used, it showed the result, which this experiment was looking for, clearly and directly. All in all, this is a simple but useful experiment, which I think absolutely meets the requirements for this project.
