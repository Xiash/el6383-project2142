
Project Review
=====================================================

## Overview

1) Briefly summarize the experiment in this project.

This project is to verify the extra latency, which is created because the
flow step process. The project designer added one controller and serveral
software OpenFlow switches to bulid a SDN topology, then the lantency and
the bandwidth between the different hosts are measured in two scenarios:
with or without the flow setup process.  Then they use ping output to get
the value of RTT to repersent the latency and use R to analyse the data.
The conclution is flow step processing will introduce
 an extra latency in the SDN.



2) Does the project generally follow the guidelines and parameters we have
learned in class?

Yes. It has a clear and specific goal, and a detailed step-by-step guide
is provided in the report, which makes the reproduction very easy.


## Experiment design

1) What is the goal of this experiment? Is it a focused, specific goal?
Is it useful and likely to have interesting results?

The goal is to verify the existence of extra latency introduced by the
flow setup process. It is specific and clear. Yes, it's useful to see
whether or not the flow setup will create extra latency and how much
latency will be added, but it would be better if they can add the words
"in software-defined network."

2) Are the metric(s) and parameters chosen appropriate for this
experiment goal? Does the experiment design clearly support the experiment goal?

Yes, the value of RTT between two hosts are used to measure latency. Using ping 
from one host to the other and obtain the output to get the RTT for every
ICMP request and reply pair. Then R Studio is used to analysis the result.

3) Is the experiment well designed to obtain maximum information with the
minimum number of trials?

Yes. The number of trials is only 1, and the result includes two situation:
with or without the flow setup processing. And that's enough for this experiment.

4) Are the metrics selected for study the *right* metrics? Are they clear,
unambiguous, and likely to lead to correct conclusions? Are there other
metrics that might have been better suited for this experiment?

Yes, to use the value of RTT is the best choice to measure latency, it's
very clear and easy to get when you use the ping output, so it is suited
for this experiment.

5) Are the parameters of the experiment meaningful? Are the ranges
over which parameters vary meaningful and representative?

Yes, the parameter is to vary the flow setup processing is used or not, it
is relative to the goal.

6) Have the authors sufficiently addressed the possibility of interactions
between parameters?

Yes, and very specific as the explain blow:

```
n this experiment, the Hard Timeout is 30 seconds.
The rules on the switch also have a Soft Timeout. It determines for how long the flow will remain in the forwarding table of the switch if there are no packets received that match the specific flow. As long as packets from that flow are received the flow remains on the flow table. The Soft Timeout is 10 seconds in this experiment. Since we keep ping for 100 times, thWe can easily notice that the first ICMP request and reply pair have a obviously longer RTT (156 ms in the above example) than others. This is because it is the first packet goes to host2, and the OVS switch itself does not know how to forward this packet to the destination at the first time. Therefore the switch must request the controller for the forwarding rule of this new flow. Since the controller and switch are connected through a public network rather than a direct link (In this experiment, the controller is at Wisconsin InstaGENI, the switch and the hosts are at NYU InstaGENI), the process will impose a manifest extra delay to this very first ICMP request and reply packet pair.
Note: Please make sure that the first RTT corresponds to the flow setup situation; i.e. it is obviously larger than others. In case if it is not, you can start pingagain after at least 10 seconds.
After instructed by the controller, the switch knows how to forward the packets from the same flow thereafter and does not need to request the controller. So the RTT of the after ICMP request and reply pairs are significantly lower than the first one.
Furthermore, we can find that there are several large RTTs for every 31 ICMP Request and Reply pair.
...
64 bytes from 10.10.1.2: icmp_req=1 ttl=64 time=156 ms
...
64 bytes from 10.10.1.2: icmp_req=32 ttl=64 time=133 ms
...
64 bytes from 10.10.1.2: icmp_req=63 ttl=64 time=151 ms
...
64 bytes from 10.10.1.2: icmp_req=94 ttl=64 time=125 ms
...
This is because all rules on the switch have a Hard Timeout. It determines the total time that a flow will remain at the forwarding table, independent of whether packets that match the flow are received; i.e. the flow will be removed after the hard timeout expires. I
e soft timeout would not expire in our execution process.
With all these above steps, we are able to verify that there do exist a significant extra latency imposed by the flow setup process.
```

7) Are comparisons made reasonably? Is the baseline selected for comparison appropriate
and realistic?

Yes, in the result it is very clear that when using flow setup, the value of RTT are significantly greater than
the situation without the flow setup. The base line is 0 and it enable to show the differece, so it is an appropriate
and realistic comparison.

## Communicating results


1) Do the authors report the quantitative results of their experiment?

Yes, the result is a graph that shows the RTT with the flow setup is more than 100ms while the
RTT without the flow setup is nearly 0ms.

2) Is there information given about the variation and/or distribution of
experimental results?

Yes, the project designer provided a simple graph result.

3) Do the authors practice *data integrity* - telling the truth about their data,
avoiding ratio games and other practices to artificially make their results seem better?

Yes, after the data are obtained, the designer explained their data specifically.

4) Is the data presented in a clear and effective way? If the data is presented in
graphical form, is the type of graph selected appropriate for the "story" that
the data is telling?

Yes, from the graph we can clearly see the difference of RTT in two situations.

5) Are the conclusions drawn by the authors sufficiently supported by the
experiment results?

Yes, the conclusion is flow setup processing used in SDN will introduce extra latency and the comparason result
 shows that clearly.

## Reproducible research



1) Did the authors include instructions for reproducing the experiment in 3 ways (Raw data -> Results,
Existing experiment setup -> Data, and Set up experiment)? Are the instructions clear
and easy to understand?

Yes, they provided the raw data, simple result, a step-by-step experiment setup, simple data, nd Set up experiment.
 It is clear and very easy to understand.

2) Were you able to successfully produce experiment results?

Yes, I produce the result successfully.

3) How long did it take you to run this experiment, from start to finish?

About 15 minutes.

4) Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?

No, I don't need to change anything expect to replace some path in the codes to my own path. To modify the path just took a few seconds.

5) In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?

5: The results can be easily reproduced by an
independent researcher with at most 15 min of
user effort, requiring only standard, freely
available tools (C compiler, etc.).


## Other comments to authors

Please write any other comments that you think might help the authors
of this project improve their experiment.
