
Project Review
=====================================================


## Overview

1) Briefly summarize the experiment in this project.
The goal of the experiment is to verify the latency induced in the network during slow setup.


2) Does the project generally follow the guidelines and parameters we have
learned in class?

Yes, this project complies with the guidelines and setups we have discussed in the class. Infact, it uses the concepts and tools given to us in the classroom.

## Experiment design

1) What is the goal of this experiment? Is it a focused, specific goal?
Is it useful and likely to have interesting results?
The goal of this experiment is to verify the extra latency induced in the
network during flow setup. This is accomplished by collecting latency data with and without flow setup and analyzing them.

2) Are the metric(s) and parameters chosen appropriate for this
experiment goal? Does the experiment design clearly support the experiment goal?
Yes, the metrics and parameters chosen for this goal are quite appropriate. The experiment design very clearly suppots the goal as 4
there can be no other way to verify the induced latency.

3) Is the experiment well designed to obtain maximum information with the
minimum number of trials?
Yes, this is an optimized experiment as it obtains maximum information with minimum types of trials necessary.

4) Are the metrics selected for study the *right* metrics? Are they clear,
unambiguous, and likely to lead to correct conclusions? Are there other
metrics that might have been better suited for this experiment?
Yes, to measure the latency induced, there can be no other way to gauge it other than RTT.
Yes, it is clear and must lead to the correct coclusion.

5) Are the parameters of the experiment meaningful? Are the ranges
over which parameters vary meaningful and representative?
yes, the parameters are meaningful. It is- whther the switch ahs tthe routing enrty for the pingpackets from one host to the other.
Since, its a there/not-there case, there is no question of the range of the parameter.

6) Have the authors sufficiently addressed the possibility of interactions
between parameters?
Since the parameters are independent and mutually exclusive, tere can be no interaction.

7) Are comparisons made reasonably? Is the baseline selected for comparison appropriate
and realistic?

Yes, the comparisons have been made quite carefully. Yes the baseline selected is the difference in latency when a routing entry exists and when it doesnt.

## Communicating results


1) Do the authors report the quantitative results of their experiment?
Yes, they report quantitative results.

2) Is there information given about the variation and/or distribution of
experimental results?
I think it would have been better if the communication of results would have been more detailed instead of rounding them-off.
I think if they would have represented more values ona line graph, it would have been better.
Also, they could have mentioned what type of TCP protocol they are using because variants like FAST, CUBIC and BIC use base RTT
which would give slightly different results.

3) Do the authors practice *data integrity* - telling the truth about their data,
avoiding ratio games and other practices to artificially make their results seem better?


4) Is the data presented in a clear and effective way? If the data is presented in
graphical form, is the type of graph selected appropriate for the "story" that
the data is telling?
As mentioned above, I think that a line graph could have given more authenticity to the results obtained in multiple trials.

5) Are the conclusions drawn by the authors sufficiently supported by the
experiment results?
yes, the conclusions drawn are fully supported by the resulstas was expected both theoretically and practically.
## Reproducible research



1) Did the authors include instructions for reproducing the experiment in 3 ways (Raw data -> Results,
Existing experiment setup -> Data, and Set up experiment)? Are the instructions clear
and easy to understand?
yes they did and the instructions are quite lucid except certain grammatical and spelling errors.

2) Were you able to successfully produce experiment results?
Yes, I could reproduce the experiment results.

3) How long did it take you to run this experiment, from start to finish?
It took me around a day to understand, read and reproduce the entire experiment.

4) Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?
Since I could not use the logins that they had provided, I had to build up the entire experiment from scratch.
It did not take me much long.

5) In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?
I think it belongs to the Third degree of reproducibility.


## Other comments to authors
there are no suggestions except that would have been better to use a line graph instead of the bar plots and that the TCP variants used should have been mentioned.
Please write any other comments that you think might help the authors
of this project improve their experiment.
